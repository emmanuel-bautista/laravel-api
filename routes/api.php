<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\TaskController;
use App\Http\Controllers\api\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('user/register', [UserController::class, 'signUp']);
Route::post('user/login', [UserController::class, 'login']);

Route::apiResource('tasks', TaskController::class)->only(['index','show']);

Route::group(['middleware'=>'auth:api'], function() {
    Route::apiResource('tasks', TaskController::class)->only(['store','update','destroy']);
});
