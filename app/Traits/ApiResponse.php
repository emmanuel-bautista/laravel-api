<?php
namespace App\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

trait ApiResponse {
    private function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }

    protected function errorResponse($message, $code)
    {
        return response()->json(
            [
                'error' => $message
            ],
            $code
        );
    }

    private function showAll(Collection $collection, $code = 200)
    {
        $collection = $collection->all();
        return $this->successResponse([
            'data' => $collection
        ], $code);
    }

    protected function showOne(Model $instance, $code = 200)
    {
        return $this->successResponse([
            'data' => $instance
        ], $code);
    }
}
